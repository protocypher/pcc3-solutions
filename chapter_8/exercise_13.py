# 8-13. User Profile:

def build_profile(first, last, **user_info):
    """Build a dictionary containing everything we know about a user."""
    user_info['first_name'] = first
    user_info['last_name'] = last
    return user_info


user_profile = build_profile(
    'John', 'Doe',
    location='Springfield',
    field='Computer Science',
    age=65,
    height=72,
    favorite_color='code'
)

print(user_profile)
