# 8-14. Cars

def build_car(manufacturer, model, **attrs):
    car = {
        'manufacturer': manufacturer,
        'model': model
    }

    for attr, value in attrs.items():
        car[attr] = value

    return car


car = build_car("toyota", "highlander", color="black", year=2002)
print(car)
