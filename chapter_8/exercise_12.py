# 8-12. Sandwiches

def make_sandwich(*toppings):
    print("Making a sandwich with:")
    for topping in toppings:
        print(f"- {topping}")


make_sandwich("cheese", "lettuce", "tomato")
make_sandwich("chicken", "pepper")
make_sandwich("brocolli")
