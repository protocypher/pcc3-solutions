# 8-7. Album

def make_album(artist, title, songs=None):
    album = {
        'artist': artist,
        'title': title
    }

    if songs:
        album['songs'] = songs

    return album


print(make_album("Pink Floyd", "The Wall"))
print(make_album("Bush", "Sixteen Stone"))
print(make_album("Counting Crows", "August and Everything After"))
print(make_album("Pink Floyd", "Animals", 5))
