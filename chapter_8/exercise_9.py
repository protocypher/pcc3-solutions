# 8-9. Messages:

def show_messages(messages):
    for message in messages:
        print(f"Message: {message}")


queue = ["One", "Two", "Three", "Four"]
show_messages(queue)
