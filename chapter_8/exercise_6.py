# 8-6. City Names:

def city_country(city, country):
    return f"{city}, {country}"


print(city_country("Boston", "USA"))
print(city_country("Cairo", "Egypt"))
print(city_country("Austin", "USA"))
