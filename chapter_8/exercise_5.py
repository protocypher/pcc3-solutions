# 8-5. Cities:

def describe_city(name, country="USA"):
    print(f"{name} is in {country}.")


describe_city("Boston")
describe_city("New York")
describe_city("Cairo", "Egypt")
