from distutils.core import setup

setup(
    name="Chapter 8",
    version="1.0",
    description="Possible Solutions for Chapter 8 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
