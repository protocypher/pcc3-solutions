# 8-10. Sending Messages:

def show_messages(queue, complete):
    while queue:
        message = queue.pop()
        print(f"Message: {message}")
        complete.append(message)


queue = ["One", "Two", "Three", "Four"]
complete = []

show_messages(queue, complete)

print(f"Queue: {queue}")
print(f"Complete: {complete}")
