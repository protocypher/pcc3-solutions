# 8-4. Large Shirts:

def make_shirt(size="large", text="I love Python"):
    print(f"A {size} shirt with the text, '{text}'.")


make_shirt()
make_shirt("medium")
make_shirt("small", "Doodle do..")
