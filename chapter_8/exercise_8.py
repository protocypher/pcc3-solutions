# 8-8. User Albums:

def make_album(artist, title, songs=None):
    album = {
        'artist': artist,
        'title': title
    }

    if songs:
        album['songs'] = songs

    return album


while True:
    artist = input("Enter an artist (or quit): ")
    if artist.lower() == "quit":
        break
    title = input("Enter an album title: ")
    print(make_album(artist, title))
