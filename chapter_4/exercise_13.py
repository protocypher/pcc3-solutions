# 4-13. Buffet

foods = ("steak", "chicken", "broccoli", "brownies", "pudding")

print("We offer:")
for food in foods:
    print(food)

# foods[0] = "not allowed"

foods = ("sushi", "chicken", "asparagus", "brownies", "pudding")

print("\nWe now offer:")
for food in foods:
    print(food)
