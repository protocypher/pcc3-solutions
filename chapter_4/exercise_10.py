# 4-10. Slices:

numbers = list(range(3, 31, 3))

for number in numbers:
    print(number)

print(f"The first three items are: {numbers[:3]}")
print(f"Three items from the middle are: {numbers[2:5]}")
print(f"The last three items are: {numbers[-3:]}")
