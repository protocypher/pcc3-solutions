# 4-11. My Pizzas, Your Pizzas

pizzas = ["pepperonni", "supreme", "cheese"]

friends_pizzas = pizzas[:]

pizzas.append("taco")
friends_pizzas.append("chicken bbq")

print("My favorite pizzas are:")
for pizza in pizzas:
    print(pizza)

print("My friend's favorite pizzas are:")
for pizza in friends_pizzas:
    print(pizza)
