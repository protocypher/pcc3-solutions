# 4-2. Animals:

animals = ["poodle", "husky", "german shepherd"]

# for animal in animals:
#     print(animal)

for animal in animals:
    print(f"A {animal} would make a great pet.")

print("These are all dogs.")
