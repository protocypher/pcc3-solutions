# 4-8. Cubes:

cubes = [x**3 for x in range(1, 11)]

for cube in cubes:
    print(cube)
