from distutils.core import setup

setup(
    name="Chapter 4",
    version="1.0",
    description="Possible Solutions for Chapter 4 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
