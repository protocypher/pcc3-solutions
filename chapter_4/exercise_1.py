# 4-1. Pizzas:

pizzas = ["pepperonni", "supreme", "cheese"]

# for pizza in pizzas:
#     print(pizza)

for pizza in pizzas:
    print(f"I like {pizza} pizza.")

print("I really like pizza.")
