# 6-6. Polling:

favorite_languages = {
    'jen': "python",
    'sara': "c",
    'edward': "rust",
    'phil': "python"
}

pollers = ["john", "jen", "jeff", "phil"]

for poller in pollers:
    if poller in favorite_languages:
        print(f"Thank you, {poller}, for taking the poll.")
    else:
        print(f"Please take the poll, {poller}.")
