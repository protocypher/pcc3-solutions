# 6-8. Pets:

pets = [
    {
        'name': "Fluffy",
        'kind': "cat",
        'owner': "John"
    }, {
        'name': "Beau",
        'kind': "dog",
        'owner': "Jane"
    }, {
        'name': "Rover",
        'kind': "rat",
        'owner': "Sally"
    }
]

for pet in pets:
    for k, v in pet.items():
        print(f"{k}: {v}")
    print()
