# 6-1. Person:

person = {
    'first_name': "Jane",
    'last_name': "Doe",
    'age': 24,
    'city': "Boston"
}

print(f"first_name = {person['first_name']}")
print(f"last_name = {person['last_name']}")
print(f"age = {person['age']}")
print(f"city = {person['city']}")
