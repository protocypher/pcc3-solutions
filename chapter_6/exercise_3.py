# 6-3. Glossary:

glossary = {
    '=': "An assignment operator.",
    'if': "A branch mechanism.",
    'else': "The catchall branch entry point.",
    'for': "Loop through lists.",
    'elif': "Coninuation branching."
}

print(f"=: {glossary['=']}")
print(f"if: {glossary['if']}")
print(f"else: {glossary['else']}")
print(f"for: {glossary['for']}")
print(f"elif: {glossary['elif']}")
