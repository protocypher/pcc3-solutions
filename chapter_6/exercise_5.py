# 6-5. Rivers:

rivers = {
    'Danube': "Europe",
    'Mississippi': "USA",
    'Nile': "Egypt",
    'Ganges': "India",
    'Missouri': "USA"
}

for river, country in rivers.items():
    print(f"The {river} runs through {country}.")
