from distutils.core import setup

setup(
    name="Chapter 6",
    version="1.0",
    description="Possible Solutions for Chapter 6 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
