# 6-11. Cities:

cities = {
    'Boston': {
        'state': "Massachusetts",
        'type': "City"
    },
    'Camden': {
        'state': "Maine",
        'type': "Town"
    },
    'Portland': {
        'state': "Maine",
        'type': "City"
    }
}

for city, info in cities.items():
    print(f"Info about {city}:")
    for k, v in info.items():
        print(f"{k}: {v}")
    print()
