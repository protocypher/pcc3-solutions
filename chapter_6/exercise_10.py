# 6-10. Favorite Numbers:

fav_nums = {
    "John": [5, 9, 6],
    "Jane": [9, 3],
    "Jeff": [12, 19],
    "Jake": [2, 4, 8],
    "Jess": [15, 18, 19],
}

for person, numbers in fav_nums.items():
    print(f"{person}'s favorite numbers are:")
    for number in numbers:
        print(number)
    print()
