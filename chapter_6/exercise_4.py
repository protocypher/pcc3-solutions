# 6-4. Glossary 2:

glossary = {
    '=': "An assignment operator.",
    'if': "A branch mechanism.",
    'else': "The catchall branch entry point.",
    'for': "Loop through lists.",
    'elif': "Coninuation branching.",
    '>': "A comparitor operator.",
    '==': "A equality operator.",
    '[]': "List brackets.",
    '{}': "Dictionary brackets.",
    '!=': "Inequality operator."
}

for term, definition in glossary.items():
    print(f"{term}: {definition}")
