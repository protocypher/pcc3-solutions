# 6-12. Extensions:

cities = {
    'Boston': {
        'state': "Massachusetts",
        'type': "City",
        'fact': "Revolution was born here."
    },
    'Camden': {
        'state': "Maine",
        'type': "Town",
        'fact': "Beautiful tourist city."
    },
    'Portland': {
        'state': "Maine",
        'type': "City",
        'fact': "The first Portland."
    }
}

for city, info in cities.items():
    print(f"Info about {city}:")
    for k, v in info.items():
        print(f"{k}: {v}")
    print()
