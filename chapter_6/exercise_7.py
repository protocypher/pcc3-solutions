# 6-7. People:

people = [
    {
        'first_name': "Jane",
        'last_name': "Doe",
        'age': 24,
        'city': "Boston"
    }, {
        'first_name': "John",
        'last_name': "Doe",
        'age': 28,
        'city': "Detroit"
    }, {
        'first_name': "Sally",
        'last_name': "Dally",
        'age': 18,
        'city': "Austin"
    }
]

for person in people:
    for k, v in person.items():
        print(f"{k}: {v}")
    print()
