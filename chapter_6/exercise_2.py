# 6-2. Favorite Number:

fav_nums = {
    "John": 5,
    "Jane": 9,
    "Jeff": 12,
    "Jake": 2,
    "Jess": 15
}

print(f"John's favorite number is: {fav_nums['John']}")
print(f"Jane's favorite number is: {fav_nums['Jane']}")
print(f"Jeff's favorite number is: {fav_nums['Jeff']}")
print(f"Jake's favorite number is: {fav_nums['Jake']}")
print(f"Jess's favorite number is: {fav_nums['Jess']}")
