# 6-9. Favorite Places:

favorite_places = {
    'John': ["Boston", "Detroit"],
    'Jane': ["Austin"],
    'Jeff': ["Disney", "Berlin", "Munich"]
}

for person, places in favorite_places.items():
    print(f"{person}'s favorite places are:")
    for place in places:
        print(place)
    print()
