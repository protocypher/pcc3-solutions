# 11-3. Employee:

class Employee:
    def __init__(self, fname, lname, sallary):
        self.first_name = fname
        self.last_name = lname
        self.sallary = sallary

    def give_raise(self, amount=None):
        amount = amount if amount else 5000
        self.sallary += amount
