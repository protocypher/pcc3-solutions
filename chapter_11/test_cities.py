# 11-1. City, Country

from city_functions import describe


def test_describe():
    city = "Boston"
    country = "USA"
    description = describe(city, country)
    assert description == "Boston, USA"
