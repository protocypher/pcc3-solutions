# 11-2. Population:

def describe(city, country, population):
    description = f"{city}, {country} - population {population}"
    return description


def describe2(city, country, population=None):
    if population:
        description = f"{city}, {country} - population {population}"
    else:
        description = f"{city}, {country}"

    return description
