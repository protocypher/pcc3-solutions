# 11-3. Employee:

import pytest
from employee import Employee


@pytest.fixture
def employee():
    e = Employee("John", "Doe", 10000)
    return e


def test_give_default_raise(employee):
    employee.give_raise()
    assert employee.sallary == 15000


def test_give_custom_raise(employee):
    employee.give_raise(2000)
    assert employee.sallary == 12000
