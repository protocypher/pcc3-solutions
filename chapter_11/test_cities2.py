# 11-2. Population:

from city_functions2 import describe, describe2


# -Uncomment out to see broken
# def test_describe():
#     city = "Boston"
#     country = "USA"
#     description = describe(city, country)
#     assert description == "Boston, USA"


def test_describe2():
    city = "Boston"
    country = "USA"
    description = describe2(city, country)
    assert description == "Boston, USA"


def test_describe2_pop():
    city = "Boston"
    country = "USA"
    description = describe2(city, country, 1000)
    assert description == "Boston, USA - population 1000"
