# Python Crash Course 3 Solutions

These are my example solutions to the exercises in the "Python Crash Course ed3" book from No Starch Press.

These are **not** official solutions, just mine to help out new learners.

I have not included any instruction material from the book, please buy a copy if you're interested. It is a great book.

