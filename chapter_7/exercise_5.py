# 7-5. Movie Tickets:

print("Please enter the ages of your party.")

while True:
    age = input("Age? ")
    if age.lower() == "quit":
        break
    age = int(age)
    if age < 3:
        print("Ticket is FREE")
    elif 3 >= age >= 12:
        print("Ticket is $10")
    else:
        print("Ticket is $12")
