# 7-10. Dream Vacation:

print("If you cold visit one place in the world, where would you go?")

places = []

while True:
    place = input("Place? ")
    if place.lower() == "quit":
        break
    places.append(place)

print("The results of the poll:")
for place in places:
    print(f"Place: {place}")
