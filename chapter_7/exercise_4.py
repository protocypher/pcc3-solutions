# 7-4. Pizza Toppings:

print("Please enter the toppings for your pizza.")

while True:
    topping = input("> ")
    if topping.lower() == "quit":
        break
    else:
        print(f"We will add {topping} to your pizza.")
