# 7-6. Three Exits:

print("Please enter the toppings for your pizza.")

while True:
    topping = input("> ")
    if topping.lower() == "quit":
        break
    else:
        print(f"We will add {topping} to your pizza.")

active = True
while active:
    topping = input("> ")
    if topping.lower() == "quit":
        active = False
    else:
        print(f"We will add {topping} to your pizza.")

topping = input("> ")
while topping.lower() != "quit":
    print(f"We will add {topping} to your pizza.")
    topping = input("> ")
