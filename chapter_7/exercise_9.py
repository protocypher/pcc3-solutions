# 7-9. No Pastrami:

sandwich_orders = [
    "pastrami",
    "tuna",
    "chciken",
    "pastrami",
    "italian",
    "pastrami"
]
finished_sandwiches = []

print("The deli has run out of pastrami")

while sandwich_orders:
    order = sandwich_orders.pop()
    if order != "pastrami":
        print(f"I have made your {order}")
        finished_sandwiches.append(order)

print(f"Finished making all: {finished_sandwiches}")
