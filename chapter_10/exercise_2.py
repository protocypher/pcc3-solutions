# 10-2. Learning C:

from pathlib import Path

path = Path("learning_python.txt")
text = path.read_text()

lines = text.splitlines()
for line in lines:
    print(line.replace("Python", "C"))
