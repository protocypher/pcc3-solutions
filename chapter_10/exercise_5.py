# 10-5. Guest Book:

from pathlib import Path

names = ""

while True:
    name = input("Please enter a guest's name (or quit): ")
    if name.lower() == "quit":
        break
    names += f"{name}\n"

path = Path("guest_book.txt")
path.write_text(names)
