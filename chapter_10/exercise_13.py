# 10-13. User Dictionary:

from pathlib import Path
import json

path = Path("user.json")

if path.exists():
    user = json.loads(path.read_text())
    print("Found user:")
    for k, v in user.items():
        print(f"- {k}: {v}")
else:
    username = input("Enter username: ")
    role = input("Enter role: ")
    age = input("Enter age: ")
    user = {
        'username': username,
        'role': role,
        'age': age
    }
    path.write_text(json.dumps(user))
