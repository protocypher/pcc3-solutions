# 10-11. Favorite Number:

from pathlib import Path
import json

path = Path("fav_num.json")
number = input("What is your favorite number? ")
path.write_text(json.dumps(number))
