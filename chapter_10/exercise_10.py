# 10-10. Common Words:

from pathlib import Path

path = Path("the_time_machine.txt")

text = path.read_text()

counts = {
    'the': text.lower().count("the"),
    'the ': text.lower().count("the ")
}

for word, count in counts.items():
    print(f"'{word}' found {count} times.")
