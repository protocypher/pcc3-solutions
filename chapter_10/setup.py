from distutils.core import setup

setup(
    name="Chapter 10",
    version="1.0",
    description="Possible Solutions for Chapter 10 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
