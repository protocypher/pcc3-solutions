# 10-8. Cats and Dogs:

from pathlib import Path

cats_path = Path("cats.txt")
dogs_path = Path("dogs.txt")

try:
    print("Cat names:")
    print(cats_path.read_text())
except FileNotFoundError as e:
    print("cats.txt not found")

try:
    print("Dog names:")
    print(dogs_path.read_text())
except FileNotFoundError as e:
    print("dogs.txt not found")
