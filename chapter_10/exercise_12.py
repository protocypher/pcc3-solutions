# 10-12. Favorite Number Remembered:

from pathlib import Path
import json

path = Path("fav_num2.json")

if path.exists():
    number = json.loads(path.read_text())
    print(f"Your favorite number is: {number}")
else:
    number = input("What is your favorite number: ")
    path.write_text(json.dumps(number))
