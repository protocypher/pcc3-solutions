# 10-1. Learning Python:

from pathlib import Path

path = Path("learning_python.txt")
text = path.read_text()

print("Printing entire contents...")
print(text)

print("Printing from a list...")
lines = text.splitlines()
for line in lines:
    print(line)

