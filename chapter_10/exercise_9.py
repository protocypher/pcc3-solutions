# 10-9. Silent Cats and Dogs:

from pathlib import Path

cats_path = Path("cats.txt")
dogs_path = Path("dogs.txt")

try:
    print("Cat names:")
    print(cats_path.read_text())
except FileNotFoundError as e:
    pass

try:
    print("Dog names:")
    print(dogs_path.read_text())
except FileNotFoundError as e:
    pass
