# 10-11. Favorite Number:

from pathlib import Path
import json

path = Path("fav_num.json")
number = json.loads(path.read_text())

print(f"I know your favorite number! It's {number}")
