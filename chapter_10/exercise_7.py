# 10-7. Addition Calculator

def get_number():
    while True:
        try:
            number = input("Enter a number: ")
            return int(number)
        except ValueError as e:
            print("Please only use digits, 0-9.")


print("Please enter two numbers to add...")
ls = get_number()
rs = get_number()
print(f"The sum of {ls} and {rs} is: {ls + rs}")
