# 10-3. Simpler Code

# We will modify exercise 10-2 instead of book samples

from pathlib import Path

path = Path("learning_python.txt")
text = path.read_text()

for line in text.splitlines():
    print(line.replace("Python", "C"))
