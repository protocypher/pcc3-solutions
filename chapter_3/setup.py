from distutils.core import setup

setup(
    name="Chapter 3",
    version="1.0",
    description="Possible Solutions for Chapter 3 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
