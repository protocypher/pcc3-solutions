# 3-2. Greetings:

names = [
    "Amber",
    "Emeli",
    "Josh",
    "Cherie"
]

print(f"Hello, {names[0]}, welcome!")
print(f"Hello, {names[1]}, welcome!")
print(f"Hello, {names[2]}, welcome!")
print(f"Hello, {names[3]}, welcome!")
