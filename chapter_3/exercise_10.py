# 3-10. Every Function:

gems = ["diamond", "ruby", "saphire", "opal", "emerald"]

print(gems)

sorted(gems)
sorted(gems, reverse=True)

gems.reverse()
gems.sort()
gems.sort(reverse=True)

del gems[0]
gems.append("tourmaline")
gems.insert(0, "jet")
gems.pop()
