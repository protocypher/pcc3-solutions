# 3-3. Your Own List:

vehicles = [
    "Jeep",
    "Scooter",
    "Snow Shoes"
]

print(f"My {vehicles[0]} can go anywhere.")
print(f"I want to use a {vehicles[1]} to go to Wiscasset.")
print(f"I can use {vehicles[2]} around the yard.")
