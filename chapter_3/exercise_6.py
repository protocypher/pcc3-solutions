# 3-6. More Guests:

invitees = [
    "Amber",
    "Josh",
    "Emeli"
]

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.\n")

print("We found more chairs in the basement!\n")

invitees.insert(0, "Cherie")
invitees.insert(2, "Jen")
invitees.append("Roland")

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.")
print(f"{invitees[3]}, you're invited to dinner.")
print(f"{invitees[4]}, you're invited to dinner.")
print(f"{invitees[5]}, you're invited to dinner.")
