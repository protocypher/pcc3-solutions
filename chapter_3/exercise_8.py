# 3-8. Seeing the World:

places = [
    "Norway",
    "Rome",
    "Nova Scotia",
    "Germany",
    "Yellowstone"
]

print(f"Normal:   {places}")
print(f"Sorted:   {sorted(places)}")
print(f"Normal:   {places}")
places.reverse()
print(f"Reversed: {places}")
places.reverse()
print(f"Normal:   {places}")
places.sort()
print(f"Sorted:   {places}")
places.sort(reverse=True)
print(f"-Sorted:  {places}")
