# 3-7. Shrinking Guest List:

invitees = [
    "Amber",
    "Josh",
    "Emeli"
]

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.\n")

print("We found more chairs in the basement!\n")

invitees.insert(0, "Cherie")
invitees.insert(2, "Jen")
invitees.append("Roland")

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.")
print(f"{invitees[3]}, you're invited to dinner.")
print(f"{invitees[4]}, you're invited to dinner.")
print(f"{invitees[5]}, you're invited to dinner.\n")

print("The new chairs are broken.\n")

name = invitees.pop()
print(f"{name}, you won't be able to join us.")

name = invitees.pop()
print(f"{name}, you won't be able to join us.")

name = invitees.pop()
print(f"{name}, you won't be able to join us.")

name = invitees.pop()
print(f"{name}, you won't be able to join us.")

print(f"{invitees[0]}, you're still invited to dinner.")
print(f"{invitees[1]}, you're still invited to dinner.")

del invitees[0]
del invitees[0]

print(invitees)

