# 3-9. Dinner Guests:

invitees = [
    "Amber",
    "Josh",
    "Emeli"
]

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.")

print(f"We've invited {len(invitees)} to dinner.")
