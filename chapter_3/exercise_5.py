# 3-5. Changing Guest List:

invitees = [
    "Amber",
    "Josh",
    "Emeli"
]

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.\n")

print(f"{invitees[1]} can't make it.\n")

invitees[1] = "Cherie"

print(f"{invitees[0]}, you're invited to dinner.")
print(f"{invitees[1]}, you're invited to dinner.")
print(f"{invitees[2]}, you're invited to dinner.")
