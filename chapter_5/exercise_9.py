# 5-9. No Users:

usernames = []

if not usernames:
    print("We need to find some users!")
else:
    for username in usernames:
        if username == "admin":
            print("Welcome, admin, you have 15 unread messages.")
        else:
            print(f"Welcome, {username}, you are logged in.")
