# 5-1. Conditional Tests:

age = 19
print("Is age > 21? False")
print(age > 21)

print("Is age > 18? True")
print(age > 18)

name = "John"
print("Is name == 'John'? True")
print(name == "John")

print("Is name == 'Sally'? False")
print(name == "Sally")

fib = [1, 2, 3, 5, 8, 13, 21]
print("Is 4 in fib? False")
print(4 in fib)

print("Is 8 in fib? True")
print(5 in fib)

char = "M"
print("Is char > 'A'? True")
print(char > "A")

print("Is char == 'Z'? False")
print(char == "Z")

pi = 3.141
print("Is pi < 2? False")
print(pi < 2)

print("Is pi >= 3? True")
print(pi >= 3)
