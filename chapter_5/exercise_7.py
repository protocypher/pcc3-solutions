# 5-7. Favorite Fruit:

fav_fruits = ["pineapple", "pomogranite", "mango", "grape"]

if "apple" in fav_fruits:
    print("I really like apples.")

if "mango" in fav_fruits:
    print("I really like mangos.")

if "orange" in fav_fruits:
    print("I really like oranges.")

if "grape" in fav_fruits:
    print("I really like grapes.")

if "pineapple" in fav_fruits:
    print("I really like pineapples")
