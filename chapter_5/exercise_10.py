# 5-10. Checking Usernames:

current_users = ["John", "Jane", "Jeff", "Jake", "Admin"]
new_users = ["jeff", "jake", "sally", "simon", "kirk"]

compare_users = [user.lower() for user in current_users]

for user in new_users:
    if user.lower() in compare_users:
        print(f"{user} is unavailable.")
    else:
        print(f"{user} is allowed.")
