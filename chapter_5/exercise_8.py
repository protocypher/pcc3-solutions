# 5-8. Hello Admin

usernames = ["john", "sally", "jeff", "jane", "admin"]

for username in usernames:
    if username == "admin":
        print("Welcome, admin, you have 15 unread messages.")
    else:
        print(f"Welcome, {username}, you are logged in.")
