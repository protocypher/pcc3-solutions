# 5-2. More Conditional Tests:

name = "John"
print(name == "Sally")
print(name == "John")

print(name.lower() == "JOHN")
print(name.lower() == "john")

x = 15
print(x == 20)
print(x == 15)

print(x != 15)
print(x != 20)

print(x > 20)
print(x > 10)

print(x < 10)
print(x < 20)

print(x >= 20)
print(x >= 15)

print(x <= 10)
print(x <= 15)

print(True and False)
print(True and True)

print(False or False)
print(True or False)

colors = ["red", "green", "blue"]
print("yellow" in colors)
print("green" in colors)

print("blue" not in colors)
print("magenta" not in colors)
