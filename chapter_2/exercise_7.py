# 2-7. Stripping Names:

person = " \t John  \n"

print(f"|{person}|")
print(f"|{person.lstrip()}|")
print(f"|{person.rstrip()}|")
print(f"|{person.strip()}|")
