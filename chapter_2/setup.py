from distutils.core import setup

setup(
    name="Chapter 2",
    version="1.0",
    description="Possible Solutions for Chapter 2 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
