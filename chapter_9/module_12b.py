# 9-12. Module for exercise 12

from module_12a import User


class Privileges:
    def __init__(self, *privileges):
        self.privileges = privileges

    def show_privileges(self):
        for privilege in self.privileges:
            print(f"- {privilege}")


class Admin(User):
    def __init__(self, fn, ln, age):
        super().__init__(fn, ln, "admin", age)
        self.privileges = Privileges("can write", "can read", "can execute")

    def show_privileges(self):
        print(f"{self.first_name} has the following privileges:")
        self.privileges.show_privileges()
