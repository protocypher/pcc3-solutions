# 9-3. Users:

class User:
    def __init__(self, fn, ln, role, age):
        self.first_name = fn
        self.last_name = ln
        self.role = role
        self.age = age

    def describe_user(self):
        print(f"{self.first_name} {self.last_name} (aged {self.age}) has the {self.role} role.")

    def greet_user(self):
        print(f"Hello, {self.first_name}")


john = User("John", "Doe", "admin", 21)
john.greet_user()
john.describe_user()

jane = User("Jane", "Doe", "devel", 38)
jane.greet_user()
jane.describe_user()

jeff = User("Jeff", "Wah", "guest", 18)
jeff.greet_user()
jeff.describe_user()

