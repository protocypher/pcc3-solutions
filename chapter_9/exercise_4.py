# 9-4. Number Served:

class Restaurant:
    def __init__(self, name, cuisine):
        self.restaurant_name = name
        self.cuisine_type = cuisine
        self.number_served = 0

    def describe_restaurant(self):
        print(f"{self.restaurant_name} has served {self.cuisine_type} to {self.number_served} customers.")

    def set_number_served(self, amount):
        self.number_served = amount

    def increment_number_served(self):
        self.number_served += 1


mcdonalds = Restaurant("McDonalds", "burgers")
mcdonalds.describe_restaurant()
mcdonalds.set_number_served(5)
mcdonalds.describe_restaurant()
mcdonalds.increment_number_served()
mcdonalds.increment_number_served()
mcdonalds.increment_number_served()
mcdonalds.increment_number_served()
mcdonalds.describe_restaurant()
