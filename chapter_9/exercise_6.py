# 9-6. Ice Cream Stand:

class Restaurant:
    def __init__(self, name, cuisine):
        self.restaurant_name = name
        self.cuisine_type = cuisine
        self.number_served = 0

    def describe_restaurant(self):
        print(f"{self.restaurant_name} has served {self.cuisine_type} to {self.number_served} customers.")

    def set_number_served(self, amount):
        self.number_served = amount

    def increment_number_served(self):
        self.number_served += 1


class IceCreamStand(Restaurant):
    def __init__(self, name):
        super().__init__(name, "deserts")
        self.flavors = ["chocolate", "vanilla", "strawberry"]

    def display_falvors(self):
        print(f"{self.restaurant_name} serves the following flavors:")
        for flavor in self.flavors:
            print(f"- {flavor}")


lindas = IceCreamStand("Linda's")
lindas.display_falvors()
