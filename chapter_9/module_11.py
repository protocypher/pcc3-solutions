# 9-11. Module for exercise 9-11

class User:
    def __init__(self, fn, ln, role, age):
        self.first_name = fn
        self.last_name = ln
        self.role = role
        self.age = age
        self.login_attempts = 0

    def describe_user(self):
        print(f"{self.first_name} {self.last_name} (aged {self.age}) has the {self.role} role.")

    def greet_user(self):
        print(f"Hello, {self.first_name}")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0


class Privileges:
    def __init__(self, *privileges):
        self.privileges = privileges

    def show_privileges(self):
        for privilege in self.privileges:
            print(f"- {privilege}")


class Admin(User):
    def __init__(self, fn, ln, age):
        super().__init__(fn, ln, "admin", age)
        self.privileges = Privileges("can write", "can read", "can execute")

    def show_privileges(self):
        print(f"{self.first_name} has the following privileges:")
        self.privileges.show_privileges()
