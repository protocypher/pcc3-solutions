# 9-13. Dice:

from random import randint


class Die:
    def __init__(self, sides=6):
        self.sides = sides

    def roll_die(self):
        roll = randint(1, self.sides)
        print(f"D{self.sides} = {roll}")


d6 = Die()

for x in range(10):
    d6.roll_die()

d10 = Die(10)

for x in range(10):
    d10.roll_die()

d20 = Die(20)

for x in range(10):
    d20.roll_die()
