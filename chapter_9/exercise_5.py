# 9-5. Login Attempts:

class User:
    def __init__(self, fn, ln, role, age):
        self.first_name = fn
        self.last_name = ln
        self.role = role
        self.age = age
        self.login_attempts = 0

    def describe_user(self):
        print(f"{self.first_name} {self.last_name} (aged {self.age}) has the {self.role} role.")

    def greet_user(self):
        print(f"Hello, {self.first_name}")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0


admin = User("John", "Do", "admin", 21)

admin.increment_login_attempts()
admin.increment_login_attempts()
admin.increment_login_attempts()
admin.increment_login_attempts()

print(admin.login_attempts)

admin.reset_login_attempts()

print(admin.login_attempts)
