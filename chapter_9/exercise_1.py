# 9-1. Restaurant:

class Restaurant:
    def __init__(self, name, cuisine):
        self.restaurant_name = name
        self.cuisine_type = cuisine

    def describe_restaurant(self):
        print(f"{self.restaurant_name} serves {self.cuisine_type}.")


restaurant = Restaurant("McDonalds", "burgers")
restaurant.describe_restaurant()
