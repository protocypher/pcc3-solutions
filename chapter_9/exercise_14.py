# 9-14. Lottery:

from random import choice


class Lottery:
    def __init__(self):
        self.source = [
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"
        ]

    def draw_ticket(self):
        lots = [
            choice(self.source),
            choice(self.source),
            choice(self.source),
            choice(self.source)
        ]

        return lots


print(Lottery().draw_ticket())
