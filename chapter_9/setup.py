from distutils.core import setup

setup(
    name="Chapter 9",
    version="1.0",
    description="Possible Solutions for Chapter 9 Exercises",
    author="Benjamin Gates",
    url="https://www.gitlab.com/protocypher/pcc3-solutions",
    packages=[]
)
