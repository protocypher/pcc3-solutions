# 9-2. Three Restaurants:

class Restaurant:
    def __init__(self, name, cuisine):
        self.restaurant_name = name
        self.cuisine_type = cuisine

    def describe_restaurant(self):
        print(f"{self.restaurant_name} serves {self.cuisine_type}.")


mcdonalds = Restaurant("McDonalds", "burgers")
mcdonalds.describe_restaurant()

burgerking = Restaurant("Burger King", "burgers")
burgerking.describe_restaurant()

tacobell = Restaurant("Taco Bell", "texmex")
tacobell.describe_restaurant()
